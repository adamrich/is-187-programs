//IS187-Project 2
//Fizz buzz by any other name
//Team 3: Sharon Schlechte, Adam Rich, Ethan Beauchamp

public class GoCards
{
    //Future proof by removing magic strings and numbers
    
    //Strings to be output when the program reaches an integer which satisfies the div_by_* conditions
    private static final String WORD_1 = "Go"; //String to be output when div_by_1 is met
    private static final String WORD_2 = "Cards!"; //String to be output when div_by_2 is met
    private static final String WORD_3 = "Go Cards!"; //String to be output when div_by_1 and div_by_2 is met
    
    //ints by which to check if the current loop iterator variable is divisible
    private static final int DIV_BY_1 = 4;
    private static final int DIV_BY_2 = 7;

    //ints at which to start and end the program's main loop
    //start_val should always be less than or equal to max_val
    private static final int START_VAL = 1;
    private static final int MAX_VAL = 100;

    public static void main(String[] args)
    {
        //Iterate over all integers from start_val to max_val inclusive
        for(int i=START_VAL; i<=MAX_VAL; i++)
        {
            //Check for double match to avoid incorrect output
            if((i%DIV_BY_1 == 0)&&(i%DIV_BY_2 == 0))
                System.out.println(WORD_3);
            else if(i%DIV_BY_1 == 0)
                System.out.println(WORD_1);
            else if(i%DIV_BY_2 == 0)
                System.out.println(WORD_2);
            else
                System.out.println(i);
        }
    }
}
