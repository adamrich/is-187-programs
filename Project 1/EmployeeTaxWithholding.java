//IS187-Project 1
//compute tax withholdings
//Team 3: Sharon Schlechte, Adam Rich, Ethan Beauchamp

import java.util.Scanner;
        
public class EmployeeTaxWithholding
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        String first_name, last_name;
        double hourly_rate, weekly_pay, withholding;

        // Get first_name, last_name, and hourly_rate 
        System.out.print("Enter Employee's First Name:\t");
        first_name = input.nextLine();

        System.out.print("Enter Employees Last Name:\t");
        last_name = input.nextLine();

        System.out.print("Enter hourly pay:\t");
        hourly_rate = input.nextDouble();

        //calculate the weekly_pay and withholding
        weekly_pay = hourly_rate * 40;
        withholding = weekly_pay * .20;

        weekly_pay = Math.round((weekly_pay*100))/100.0; //Round weekly_pay to two decimal places
        withholding = Math.round((withholding*100))/100.0; //Round withholding to two decimal places

        //Title
        System.out.println("WITHHOLDING FOR EACH EMPLOYEE\n");

        //Column headings
        System.out.println("First Name" + "\t" + "Last Name" + "\t" + "Hourly Rate" + "\t" + "Weekly Pay" + "\t" + "Withholding Amount");
        
        System.out.println(first_name + "\t" + last_name + "\t\t" + hourly_rate + "\t\t" + weekly_pay + "\t\t" + withholding);
        System.out.println("...\n");
        System.out.println("END OF REPORT");
    }
    
}
