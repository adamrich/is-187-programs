//IS187-Project 3
//Lookup and output the wind chill from given inputs
//Team 3: Adam Rich, Ethan Beauchamp

import java.util.Scanner;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileNotFoundException;

public class AKWindChill
{
    private static final String FILE_NAME = "temperature.txt";

    //This is the character that is used to sepatate values in the file
    private static final String FILE_DELIMITER = "\t";

    //This is the increment of the axes in the file.
    //In the event of a different increment, you will need to test and change
    //the formulas for the array index values
    private static final int FILE_INCRE = 5;

    private static final int MIN_TEMP = -20;
    private static final int MAX_TEMP = 15;
    private static final int TEMP_RANGE = MAX_TEMP - MIN_TEMP; 

    private static final int MIN_WIND_VELO = 5;
    private static final int MAX_WIND_VELO = 30;
    private static final int WIND_VELO_RANGE = MAX_WIND_VELO - MIN_WIND_VELO;

    //These constants are used when calling printline()
    private static final char LINE_CHAR = '-';
    private static final char LINE_LENGTH = 50;

    public static void main(String[] args)
    {
        Scanner stdin = new Scanner(System.in);
        BufferedReader fin = null; 

        int wind_chill_table[][] = new int[TEMP_RANGE][WIND_VELO_RANGE];
        String[] table_arr;
        boolean quit;

        int temp, wind_velo; //input variables
        
        try
        {
            fin = new BufferedReader(new FileReader(FILE_NAME));
        }
        catch(FileNotFoundException e) //Exit in case
        {
            System.out.println("Wind chill table file does not exist!");
            System.exit(0);
        }

        //This is not the best way to handle reading in the file,
        //as it does not validate the file structure.

        //This works well, assuming that the table will always be formatted
        //correctly.
        try
        {
            for(int i=0; i<=TEMP_RANGE/FILE_INCRE; i++)
            {
                table_arr = fin.readLine().split(FILE_DELIMITER);

                for(int j=0; j<=WIND_VELO_RANGE/FILE_INCRE; j++)
                {
                    wind_chill_table[i][j] = Integer.parseInt(table_arr[j]);
                }
            }
        }
        catch(IOException e)
        {
            System.out.println("Error reading file");
        }

        do //Begin main program loop
        {
            printline(LINE_LENGTH, LINE_CHAR);
            
            System.out.print("Enter a Temperature divisible by " + FILE_INCRE
                             + " between " + MIN_TEMP + "F and " + MAX_TEMP
                             + "F inclusive: ");
            temp = stdin.nextInt();
            
            System.out.print("Enter a Wind Velocity between " + MIN_WIND_VELO
                             + "MPH and " + MAX_WIND_VELO + "MPH inclusive: ");
            wind_velo = stdin.nextInt();

            //Check that both of the inputs are valid before printing a result.
            if(validate_input(temp, MAX_TEMP, MIN_TEMP, FILE_INCRE)
               && validate_input(wind_velo, MAX_WIND_VELO, MIN_WIND_VELO, FILE_INCRE))
                //These formulas have only been tested using FILE_INCRE = 5
                System.out.println("The wind chill at " + temp + "F and "
                                   + wind_velo + "MPH is " + wind_chill_table[temp/FILE_INCRE+FILE_INCRE-1][(wind_velo-FILE_INCRE)/FILE_INCRE]);

            System.out.print("Would you like to exit? (y/n): ");
            //Assume that the user does not want to quit
            quit = stdin.next().toLowerCase().charAt(0) == 'y';
        }while(!quit);

        //Close the file reader to clean up before exiting
        try
        {
            fin.close(); //Clean up
        }
        catch(IOException e)
        {
            System.out.println("Error closing file");
        }
    }
    
    //Print a line of char c, int length long
    //Then ouput a newline
    private static void printline(int length, char c)
    {
        for(int i=0; i<length; i++)
            System.out.print(c);

        System.out.println();
    }

    //Validate the input, make sure max > input > min and input is divisible by
    //increment
    private static boolean validate_input(int input, int max, int min,
                                          int increment)
    {
        boolean not_valid = ((input > max) || (input < min) || (input % increment != 0));
        if(not_valid) //This print statement might be better served by being put in the statement that calls validate_input()
                    System.out.println("Input out of range. Please try again.\n");
        return !not_valid;
    }
}
