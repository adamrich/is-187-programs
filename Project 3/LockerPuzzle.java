//IS187-Project 3
//Find the open lockers
//Team 3: Sharon Schlechte, Adam Rich, Ethan Beauchamp

public class LockerPuzzle
{
    public static void main(String[] args)
    {
        /*boolean arrays are instantiated with every value as false,
          so no need to "fix" this by adding a loop to instantiate each element
          as false */
        boolean[] locker = new boolean[100];

        //Started the folowing loops at one to avoid issues with divide by 0
        for(int i=1; i<=100; i++) //loop for each student
            for(int j=1; j<=100; j++) //loop for each locker for each student
                if(j%i == 0)
                    locker[j-1] = !locker[j-1];

        for(int i=0; i<100; i++) //print loop
            if(locker[i]) //ignore the closed lockers
                System.out.println("Locker " + (i+1) + " is open");
    }
}
